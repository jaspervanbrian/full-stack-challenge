import Chart from 'chart.js';

class PieChart {
  constructor(ctx, labels, data) {
    this.chart = new Chart(ctx, {
      type: 'pie',
      data: {
        labels,
        datasets: [
          {
            data: [data.ai, data.jr, data.sz],
            backgroundColor: [
              '#E289F2',
              '#263238',
              '#855CF8',
            ],
          },
        ],
      },
      options: {
        legend: {
          display: true,
          animateScale: true,
        },
      },
    });
  }

  addUser(label, quality) {
    if (label === 'ai') {
      this.chart.data.datasets[0].data[0] += quality;
    } else if (label === 'jr') {
      this.chart.data.datasets[0].data[1] += quality;
    } else if (label === 'sz') {
      this.chart.data.datasets[0].data[2] += quality;
    }

    this.chart.update();
  }
}

export default {
  mounted() {
    this.graph = null;

    this.handleEvent('chart-data-init', ({ data: chartData }) => {
      const { labels, data } = chartData;
      this.graph = new PieChart(this.el, labels, data);
    });

    this.handleEvent('new-user', ({ label, quality }) => {
      this.graph.addUser(label, quality);
    });
  },
};
