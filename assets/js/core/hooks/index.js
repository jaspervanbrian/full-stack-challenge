import UserQualityGraph from './UserQualityGraph';

const Hooks = {
  connection: {
    mounted() {
    },

    updated() {
    },
  },

  UserQualityGraph,
};

export default Hooks;
