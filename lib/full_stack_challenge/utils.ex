defmodule FullStackChallenge.Utils do
  @doc """
  Helper function to parse a numeric string to integer or float

  ## Examples

    parse_integer_or_float("1")
    #=> 1

    parse_integer_or_float("1.1")
    #=> 1.1

    parse_integer_or_float("hello world")
    #=> {:error, :invalid_format}
  """
  def parse_integer_or_float(numeric_string) when is_bitstring(numeric_string) do
    case Integer.parse(numeric_string) do
      {num, ""} ->
        num

      {_num, _} ->
        case Float.parse(numeric_string) do
          {num, ""} -> num
          _ -> {:error, :invalid_format}
        end

      _ ->
        {:error, :invalid_format}
    end
  end

  def parse_integer_or_float(int) when is_integer(int), do: int
  def parse_integer_or_float(float) when is_float(float), do: float
end
