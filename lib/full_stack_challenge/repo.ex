defmodule FullStackChallenge.Repo do
  use Ecto.Repo,
    otp_app: :full_stack_challenge,
    adapter: Ecto.Adapters.Postgres
end
