defmodule FullStackChallenge.Accounts.User do
  @derive {Jason.Encoder, only: ~w[name quality]a}

  use Ecto.Schema
  import Ecto.Changeset

  alias FullStackChallenge.Utils

  schema "users" do
    field :name, :string
    field :quality, :float

    timestamps()
  end

  @doc false
  def changeset(%__MODULE__{} = user, attrs) do
    user
    |> cast(attrs, [:name, :quality])
    |> validate_required([:name, :quality])
    |> validate_numeric_string(:quality)
  end

  defp validate_numeric_string(changeset, field) do
    if numeric_string = get_field(changeset, field) do
      case Utils.parse_integer_or_float(numeric_string) do
        {:error, _} ->
          add_error(changeset, field, "Format invalid")

        num when is_integer(num) or is_float(num) ->
          changeset
      end
    else
      changeset
    end
  end
end
