defmodule FullStackChallengeWeb.PageLive do
  use FullStackChallengeWeb, :live_view

  alias Phoenix.LiveView.Socket
  alias FullStackChallenge.{
    Accounts,
    Accounts.User,
    Utils
  }

  @impl true
  def mount(_params, _session, socket) do
    users = Accounts.list_users()

    chart_data = chart_data_init(users)


    socket =
      socket
      |> push_event("chart-data-init", %{data: chart_data})
      |> assign(users: users)
      |> assign_changeset()

    {:ok, socket}
  end

  @impl true
  def handle_event("validate", %{"user" => %{"name" => name, "quality" => quality}}, socket) do
    changeset = User.changeset(%User{}, %{name: name, quality: quality})

    {:noreply, update(socket, :changeset, fn _ -> changeset end)}
  end

  @impl true
  def handle_event("create_user", %{"user" => user_attrs}, socket) do
    {:noreply, create_user(socket, user_attrs)}
  end

  defp chart_data_init(users) do
    data =
      users
      |> Enum.reduce(%{ai: 0, jr: 0, sz: 0}, fn user, acc ->
        first_letter_as_int = user.name |> String.to_charlist |> hd
        parsed_user_quality = Utils.parse_integer_or_float(user.quality)

        cond do
          first_letter_as_int in ?A..?I ->
            Map.replace(acc, :ai, acc[:ai] + parsed_user_quality)
          first_letter_as_int in ?a..?i ->
            Map.replace(acc, :ai, acc[:ai] + parsed_user_quality)
          first_letter_as_int in ?J..?R ->
            Map.replace(acc, :jr, acc[:jr] + parsed_user_quality)
          first_letter_as_int in ?j..?r ->
            Map.replace(acc, :jr, acc[:jr] + parsed_user_quality)
          first_letter_as_int in ?S..?Z ->
            Map.replace(acc, :sz, acc[:sz] + parsed_user_quality)
          first_letter_as_int in ?s..?z ->
            Map.replace(acc, :sz, acc[:sz] + parsed_user_quality)
          true -> acc
        end
      end)

    %{
      labels: ["A-I", "J-R", "S-Z"],
      data: data
    }
  end

  defp assign_changeset(socket) do
    changeset = User.changeset(%User{}, %{})
    assign(socket, changeset: changeset)
  end

  defp create_user(%Socket{assigns: assigns} = socket, %{"name" => name, "quality" => quality}) do
    if assigns.changeset.valid? do
      {:ok, user} = Accounts.create_user(%{name: name, quality: quality})
      first_letter_as_int = user.name |> String.to_charlist |> hd

      label =
        cond do
          first_letter_as_int in ?A..?I -> "ai"
          first_letter_as_int in ?a..?i -> "ai"
          first_letter_as_int in ?J..?R -> "jr"
          first_letter_as_int in ?j..?r -> "jr"
          first_letter_as_int in ?S..?Z -> "sz"
          first_letter_as_int in ?s..?z -> "sz"
          true -> ""
        end

      socket
      |> push_event("new-user", %{label: label, quality: user.quality})
      |> assign_changeset()
      |> update(:users, fn users -> [user | users] end)
    else
      socket
    end
  end
end
