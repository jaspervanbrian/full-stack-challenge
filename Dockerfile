FROM elixir:1.11.4

RUN apt-get update && \
    apt-get install -y postgresql-client && \
    apt-get install -y inotify-tools && \
    curl -sL https://deb.nodesource.com/setup_14.x | sh && \
    apt-get install -y nodejs && \
    curl -L https://npmjs.org/install.sh | sh && \
    mix local.hex --force && \
    mix archive.install hex phx_new 1.5.8 --force && \
    mix local.rebar --force

ENV APP_HOME /app
COPY . $APP_HOME/
WORKDIR $APP_HOME

RUN mix deps.get
RUN mix deps.compile
RUN cd assets && npm install && node node_modules/webpack/bin/webpack.js --mode development
RUN mix compile
EXPOSE 4000

RUN chmod +x $APP_HOME/docker_entrypoint.sh

CMD ["/app/docker_entrypoint.sh"]
