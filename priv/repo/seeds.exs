# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     FullStackChallenge.Repo.insert!(%FullStackChallenge.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

date_now = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)

FullStackChallenge.Repo.insert_all(FullStackChallenge.Accounts.User, [
  %{name: "Aldo Raine", quality: 8.0, inserted_at: date_now, updated_at: date_now},
  %{name: "Boba Fett", quality: 8.0, inserted_at: date_now, updated_at: date_now},
  %{name: "Frank Furter", quality: 9.0, inserted_at: date_now, updated_at: date_now},
  %{name: "Marsellus Wallace", quality: 25.0, inserted_at: date_now, updated_at: date_now},
  %{name: "Sofie Fatale", quality: 100.0, inserted_at: date_now, updated_at: date_now},
  %{name: "Zed", quality: 50.0, inserted_at: date_now, updated_at: date_now}
])
